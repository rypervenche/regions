// TODO: Move the choice of user_list vs over_limit to a function and reduce number of commands in half
// TODO: Have errors in main detach from docker session instead of panicking
// TODO: Add temp regions that get removed after 2 weeks
//       maybe with cronjob or on next run
// TODO: Make command possible via CLI instead of interactive loop

use ansi_term::Color::{Green, Red, Yellow};
use anyhow::{anyhow, bail, Result};
use clap::Parser;
use once_cell::sync::Lazy;
use regex::Regex;
use serde_yaml::Value;
use std::io::Write;
use std::path::Path;
use std::sync::Mutex;
use std::thread::{self, sleep};
use std::time::Duration;
use std::{
    collections::BTreeMap,
    fmt::Display,
    fs, io,
    process::{Command, Stdio},
};

const REGION_YAML_FILE: &str = "/pro/minecraft-1/plugins/WorldGuard/worlds/Earth/regions.yml";
const UUID_DB: &str = "/pro/minecraft-1/plugins/WorldGuard/cache/profiles.sqlite";
const OUTPUT_FILE: &str = "/tmp/list.txt";
const DEFAULT_REGION_LIMIT: usize = 4;
const SLEEP_MILLIS: u64 = 250;

static REGION_YAML: Lazy<String> = Lazy::new(|| fs::read_to_string(REGION_YAML_FILE).unwrap());
static YAML: Lazy<Mutex<Value>> =
    Lazy::new(|| Mutex::new(serde_yaml::from_str(&REGION_YAML).unwrap()));
static RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"(?m)(?:([a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}): [^\n]+\n((?:(?:[^\n]+)(?:\n|$))*))+").unwrap()
});

fn update_yaml_input() -> Result<()> {
    let mut yaml = YAML.lock().unwrap();
    let string = fs::read_to_string(REGION_YAML_FILE).unwrap();
    let new_yaml = serde_yaml::from_str(&string).unwrap();
    *yaml = new_yaml;
    Ok(())
}

static OPTS: Lazy<Opts> = Lazy::new(Opts::parse);

#[derive(Parser)]
#[clap(version, about = "Show users with too many regions")]
struct Opts {
    /// Remove color
    #[clap(short, long)]
    no_color: bool,

    /// Save to file
    #[clap(short, long)]
    save_to_file: bool,

    /// Max number of regions allowed
    #[clap(short = 'l', long)]
    region_limit: Option<usize>,

    /// Update regions from file
    #[clap(short, long)]
    from_file: Option<String>,

    /// Output empty regions for all users
    #[clap(short, long, requires("save_to_file"))]
    empty_regions: bool,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone)]
struct User {
    id: String,
    name: String,
    regions: Vec<String>,
}

impl User {
    fn is_over_limit(&self, limit: usize) -> bool {
        match self.regions.len() {
            0 => true,
            n if n > 0 && n > limit => true,
            n if n > 0 && n <= limit => false,
            _ => panic!("Number of regions is not correct."),
        }
    }

    fn add_remove_regions(&self, keep_regions: Option<&Vec<String>>) -> Result<()> {
        println!("\nUser: {}", self.name);
        match keep_regions {
            Some(keep) => {
                for region in &self.regions {
                    if !keep.contains(&region.to_string()) {
                        print_region_change(region, false);
                        tmux_send_command(&format!(
                            "rg removemember -w Earth {region} {}",
                            &self.id
                        ))?;
                        sleep(Duration::from_millis(SLEEP_MILLIS));
                    }
                }
                for new in keep {
                    if !self.regions.contains(new) {
                        print_region_change(new, true);
                        tmux_send_command(&format!("rg addmember -w Earth {new} {}", &self.id))?;
                        sleep(Duration::from_millis(SLEEP_MILLIS));
                    }
                }
            }
            None => {
                for region in &self.regions {
                    print_region_change(region, false);
                    tmux_send_command(&format!("rg removemember -w Earth {region} {}", &self.id))?;
                    sleep(Duration::from_millis(SLEEP_MILLIS));
                }
            }
        }
        Ok(())
    }
}

impl Display for User {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let name = if OPTS.save_to_file || OPTS.no_color {
            self.name.to_string()
        } else {
            Yellow.bold().paint(&self.name).to_string()
        };

        let regions = self.regions.join("\n");
        if regions.is_empty() || OPTS.empty_regions {
            writeln!(f, "{}: {}", self.id, name)
        } else {
            writeln!(f, "{}: {}\n{regions}", self.id, name)
        }
    }
}

fn get_input() -> Result<String> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let input = input.trim().to_string();
    Ok(input)
}

fn create_uuid_list() -> Result<String> {
    let command = Command::new("sqlite3")
        .args([UUID_DB, "SELECT * FROM uuid_cache;"])
        .stdout(Stdio::piped())
        .output()?;
    let output = String::from_utf8(command.stdout)?;
    Ok(output)
}

fn print_region_change(region: &str, add: bool) {
    let region = Yellow.bold().paint(region);
    let action = match add {
        true => Green.bold().paint("Adding"),
        false => Red.bold().paint("Removing"),
    };
    println!("{action} region {region}...");
}

fn start_tmux_session() -> Result<()> {
    let _tmux_new = Command::new("tmux")
        .args(["new", "-s", "atwmc_regions", "-d"])
        .status()?;
    let attach = Command::new("tmux")
        .args([
            "send-keys",
            "-t",
            "atwmc_regions",
            "docker attach pro-minecraft-1",
            "C-m",
        ])
        .status()?;

    if attach.success() {
        Ok(())
    } else {
        bail!("Failed to attach to tmux session")
    }
}
fn end_tmux_session() -> Result<()> {
    let attach = Command::new("tmux")
        .args(["send-keys", "-t", "atwmc_regions", "C-p", "C-q"])
        .status()?;

    if attach.success() {
        Ok(())
    } else {
        bail!("Failed to end tmux session")
    }
}

fn tmux_send_command(command: &str) -> Result<()> {
    let remove_region = Command::new("tmux")
        .args(["send-keys", "-t", "atwmc_regions", command, "C-m"])
        .status()?;

    if remove_region.success() {
        Ok(())
    } else {
        bail!("Failed to remove user")
    }
}

fn create_id_map(id_map: String) -> BTreeMap<String, String> {
    let mut hashmap = BTreeMap::new();
    for line in id_map.lines() {
        let (id, name) = line.split_once('|').unwrap();
        hashmap.insert(id.to_owned(), name.to_owned());
    }
    hashmap
}

fn print_to_file(over_limit: &[User]) -> Result<()> {
    let output = over_limit
        .iter()
        .map(|u| u.to_string())
        .collect::<Vec<_>>()
        .join("\n");

    fs::write(OUTPUT_FILE, output)?;

    println!("Saved to \"{OUTPUT_FILE}\"");

    Ok(())
}

fn choose_user(list: &[User]) -> Result<User> {
    for (i, user) in list.iter().enumerate() {
        println!("{}: {}", i + 1, user.name);
    }

    println!();

    let choice: usize = get_input()?.parse()?;

    println!();

    list.get(choice - 1)
        .ok_or_else(|| anyhow!("User not found."))
        .cloned()
}

fn print_users_over_limit(user_list: &[User]) {
    for user in user_list {
        println!("{user}");
    }
}

fn print_totals(user_list: &[User], over_limit: &[User], region_limit: usize) {
    let total_users = Green.bold().paint(user_list.len().to_string()).to_string();
    let users_over = Red.bold().paint(over_limit.len().to_string()).to_string();
    let limit = Green.bold().paint(region_limit.to_string()).to_string();

    println!("{} total users with regions", total_users);
    println!("{} users over limit of {}", users_over, limit);
}

fn get_regions() -> Result<Vec<String>> {
    print!("Type regions separated by commas: ");
    io::stdout().flush()?;
    let regions = get_input()?;
    let regions = regions.split(',').map(|s| s.to_owned()).collect::<Vec<_>>();
    Ok(regions)
}

fn create_user_list() -> Result<Vec<User>> {
    let mut final_list = Vec::new();
    let uuid_list = create_uuid_list()?;
    let id_map = create_id_map(uuid_list);
    let yaml = YAML.lock().unwrap();
    let regions = yaml["regions"].as_mapping().expect("Can't make a mapping");
    // dbg!(&id_map);
    for id in id_map.keys() {
        let mut user_regions = Vec::new();
        for (region, inside) in regions.iter() {
            let region = region.as_str().unwrap().to_owned();
            if let Some(ids) = inside["members"]["unique-ids"].as_sequence() {
                let ids: Vec<_> = ids.iter().map(|v| v.as_str().unwrap().to_owned()).collect();
                if ids.contains(id) {
                    user_regions.push(region);
                }
            }
        }
        let name = &id_map[id];
        let user = User {
            id: id.to_string(),
            name: name.to_owned(),
            regions: user_regions,
        };
        final_list.push(user);
    }
    final_list.sort_by(|a, b| {
        a.name
            .to_lowercase()
            .partial_cmp(&b.name.to_lowercase())
            .unwrap()
    });
    Ok(final_list)
}

fn read_updates_file(file: impl AsRef<Path>) -> Result<BTreeMap<String, Vec<String>>> {
    let content = fs::read_to_string(file)?;

    let mut captures_map: BTreeMap<String, Vec<String>> = BTreeMap::new();

    for caps in RE.captures_iter(&content) {
        let key = caps.get(1).map_or("", |m| m.as_str()).to_string();
        let mut values = Vec::new();
        if let Some(value) = caps.get(2) {
            let value = value.as_str().trim();
            values = value.split('\n').map(|s| s.to_string()).collect();
        }

        captures_map.entry(key).or_default().extend(values);
    }
    Ok(captures_map)
}

fn main() -> Result<()> {
    start_tmux_session()?;

    let region_limit = match (&OPTS.from_file, OPTS.region_limit) {
        (Some(_), _) => 0,
        (None, Some(l)) => l,
        (None, None) => DEFAULT_REGION_LIMIT,
    };

    loop {
        update_yaml_input()?;
        let user_list = create_user_list()?;
        let over_limit: Vec<_> = user_list
            .clone()
            .into_iter()
            .filter(|u| u.is_over_limit(region_limit))
            .collect();

        if let Some(file) = &OPTS.from_file {
            let map = read_updates_file(file)?;
            for (uuid, regions) in map.iter() {
                let found_user = user_list.iter().find(|e| e.id == *uuid);
                if let Some(user) = found_user {
                    let fixed_regions = if regions.len() == 1 && regions[0].is_empty() {
                        None
                    } else if !regions.is_empty() {
                        Some(regions)
                    } else {
                        None
                    };
                    user.add_remove_regions(fixed_regions)?
                }
            }
            break;
        }

        if OPTS.save_to_file {
            print_to_file(&over_limit)?;
            break;
        }

        println!("What would you like to do?");
        println!();
        println!("p  | Print single user and their current regions. (over limit)");
        println!("P  | Print single user and their current regions. (full list)");
        println!("l  | List all users and their current regions. (over limit)");
        println!("L  | List all users and their current regions. (full list)");
        println!("s  | Set regions for a user. (over limit)");
        println!("S  | Set regions for a user. (full list)");
        println!("r  | Remove all regions from a user. (over limit)");
        println!("R  | Remove all regions from a user. (full list)");
        println!("q  | Quit.");
        println!();

        let input = get_input()?;
        println!();

        match input.as_ref() {
            "p" => {
                let user = match choose_user(&over_limit) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
            }
            "P" => {
                let user = match choose_user(&user_list) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
            }
            "l" => {
                print_users_over_limit(&over_limit);
                print_totals(&user_list, &over_limit, region_limit);
            }
            "L" => {
                print_users_over_limit(&user_list);
                print_totals(&user_list, &over_limit, region_limit);
            }
            "s" => {
                let user = match choose_user(&over_limit) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
                let regions = get_regions()?;
                user.add_remove_regions(Some(&regions))?;
                tmux_send_command("rg save")?;
                thread::sleep(Duration::from_secs(2));
            }
            "S" => {
                let user = match choose_user(&user_list) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
                let regions = get_regions()?;
                user.add_remove_regions(Some(&regions))?;
                tmux_send_command("rg save")?;
                thread::sleep(Duration::from_secs(2));
            }
            "r" => {
                let user = match choose_user(&over_limit) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
                println!("The above regions will be removed. Proceed? y/N");
                let answer = get_input()?;
                match answer.as_ref() {
                    "y" | "Y" => (),
                    _ => continue,
                }
                user.add_remove_regions(None)?;
                tmux_send_command("rg save")?;
                thread::sleep(Duration::from_secs(2));
            }
            "R" => {
                let user = match choose_user(&user_list) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                println!("{user}");
                println!("The above regions will be removed. Proceed? y/N");
                let answer = get_input()?;
                match answer.as_ref() {
                    "y" | "Y" => (),
                    _ => continue,
                }
                user.add_remove_regions(None)?;
                tmux_send_command("rg save")?;
                thread::sleep(Duration::from_secs(2));
            }
            "q" => break,
            _ => continue,
        }
    }

    end_tmux_session()?;

    Ok(())
}
